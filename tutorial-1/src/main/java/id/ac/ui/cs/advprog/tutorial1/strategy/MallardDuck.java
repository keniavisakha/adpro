package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    /*FlyBehavior flybehaviour;
    QuackBehavior quackbehaviour;*/

    public MallardDuck(){
        /*quackbehaviour = new Quack();
        flybehaviour = new FlyWithWings();*/

        setFlyBehavior(new FlyWithWings());
        setQuackBehavior(new Quack());
    }

    public void display(){
        System.out.println("I look like Mallard Duck! :)");
    }
}
