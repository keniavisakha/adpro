package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {
    private FlyBehavior fb;
    private QuackBehavior qb;

//    public FlyBehavior fb;
//    public QuackBehavior qb;

    public Duck(){ }

    public void performFly() {
        fb.fly();
    }

    public void performQuack() { qb.quack();}

    // TODO Complete me!

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.fb = flyBehavior;
    }

    public void setQuackBehavior(QuackBehavior quackquack) { this.qb = quackquack;}

    public void swim(){
        System.out.println("Every Duck can float :)");
    }

    public abstract void display();

    /* These method create problem since not all duck can fly and quack
    public void fly(){System.out.println("The duck FLY!!");}
    public void quack(){System.out.println("Quack");}

    write notes down next time
    */
}
