package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    // TODO Complete me!
    FlyBehavior flybehavior;
    QuackBehavior quackbehaviour;

    public ModelDuck(){
       /* quackbehaviour = new MuteQuack();
        flybehavior = new FlyNoWay();*/
       setFlyBehavior(new FlyNoWay());
       setQuackBehavior(new MuteQuack());
    }

    public void display(){
        System.out.println("I look like model Duck!");
    }

}
